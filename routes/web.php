<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductoController;
//use CloudinaryLabs\CloudinaryLaravel\MediaAlly;//va en el modelo
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/producto', function () {
    return view('producto.index');
});

//Route::get('/producto/create',[ProductoController::class,'create']);

Route::resource('producto', ProductoController::class);
Route::get('/producto/ver',[ProductoController::class,'ver'])->name('producto.ver');

Route::get('upload', function () {
    return view('upload');
});
Route::post('upload', function (Request $request) {
    $uploadedFileUrl = Cloudinary::upload($request->file('imagen')->getRealPath(), [
        'folder' => 'Testting', //guardo IMG en la carpeta Tesstting, si esta carpeta no existe "lo crea"
    ])->getSecurePath();
    //dd($uploadedFileUrl);
});