{{--<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>CRUD</title>
        <!-- BOOTSTRAP 5 -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

         <!-- Font Awesome CDN-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

    </head>
    <nav class="navbar navbar-dark bg-dark mb-4">
        <a href="/producto" class="navbar-brand">CRUD Mongo</a>
        <a href="{{ route('producto.show', 1) }}" class="navbar-brand">Landing</a>
    </nav>
    <body>
--}}
@extends('layout.app')

@push('css')
    <style>
    </style>
@endpush
@section('content')
<div class="container">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Productos</h1>
        
      </div>
      <div>
        <table class="table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Precio</th>
                    <th>Stock</th>
                    <th>Estado</th>
                    <th scope="col" style="text-align:center"><a href="{{ route('producto.create') }}" class="h3"><i class="fas fa-plus-square text-success"></i></a></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($productos as $producto)
                    <tr>
                        <td>{{ $producto->nombre }}</td>
                        <td>{{ $producto->description }}</td>
                        <td>{{ $producto->precio }}</td>
                        <td>{{ $producto->stock }}</td>
                        <td>{{ $producto->estado }}</td>
                        <td>
                            <a href="{{ route('producto.show', $producto) }}" class="mr-2 border-0"><i class="far fa-eye text-info"></i></a>
                            <a href="{{ route('producto.edit', $producto) }}" class="mr-2 border-0"><i class="fas fa-pencil-alt text-warning"></i></a>
                            <form action="{{ route('producto.destroy', $producto) }}" method="post" style="margin:0px;">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="mr-2 border-0" title="Eliminar"
                                    onclick="return confirm('¿Desea eliminar este registro?')">
                                    <i class="fas fa-trash-alt text-danger" aria-hidden="true"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
      </div>  
</div>
@endsection

{{--

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>
--}}
