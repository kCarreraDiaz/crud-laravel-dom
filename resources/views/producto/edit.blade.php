{{--<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>CRUD - MOGO</title>
        <!-- BOOTSTRAP 5 -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

         <!-- Font Awesome CDN-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    </head>
    <nav class="navbar navbar-dark bg-dark mb-4">
        <a href="/producto" class="navbar-brand">CRUD Mongo</a>
        <a href="{{ route('producto.show', 1) }}" class="navbar-brand">Landing</a>
    </nav>
    <body>--}}
@extends('layout.app')

@push('css')
<style>
</style>
@endpush
@section('content')
<div class="container">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Editar producto</h1>
    </div>
    <div class="card mb-4">
        <div class="card-body">
            <h3 style="color:#011344;">EDITAR PRODUCTO</h3>
            <form class="" action="{{ route('producto.update', $producto) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input type="hidden" name="lastImage" value="{{ old('', $producto->imagen) }}">
                <div class="form-group py-2">
                    <div class="form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                        <strong><label style="color:#0C2054;">Nombre</label></strong>
                        <input type="text" name="nombre" value="{{ old('nombre', $producto->nombre) }}" class="form-control" placeholder="Ingrese el nombre" required />
                        @if ($errors->has('nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group py-2 {{ $errors->has('description') ? ' has-error' : '' }}">
                        <strong><label style="color:#0C2054;">description</label></strong>
                        <input type="text" name="description" value="{{ old('description', $producto->description) }}" class="form-control" placeholder="Ingrese descripcion" required />
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group py-2 {{ $errors->has('precio') ? ' has-error' : '' }}">
                        <strong><label style="color:#0C2054;">Precio</label></strong>
                        <input type="text" name="precio" value="{{ old('precio', $producto->precio) }}" class="form-control" placeholder="Ingrese el precio" required />
                        @if ($errors->has('precio'))
                            <span class="help-block">
                                <strong>{{ $errors->first('precio') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group py-2 {{ $errors->has('stock') ? ' has-error' : '' }}">
                        <strong><label style="color:#0C2054;">Stock</label></strong>
                        <input type="text" name="stock" value="{{ old('stock', $producto->stock) }}" class="form-control" placeholder="Ingrese el stock" required />
                        @if ($errors->has('stock'))
                            <span class="help-block">
                                <strong>{{ $errors->first('stock') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group py-2 {{ $errors->has('estado') ? ' has-error' : '' }}">
                        <strong><label style="color:#0C2054;">Estado</label></strong>
                        <input type="text" name="estado" value="{{ old('estado', $producto->estado) }}" class="form-control" placeholder="Ingrese el estado" required />
                        @if ($errors->has('estado'))
                            <span class="help-block">
                                <strong>{{ $errors->first('estado') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group py-2">
                        <input type="file" name="imagenNueva[]" multiple placeholder="" class="form-control">
                    </div>
                    <div class="panel-footer text-center py-4">
                        <input type="submit" class="btn btn-primary" value="Guardar" style="width:300px;" data-loading-text="Guardando..."/>
                        <a href="{{ route('producto.index') }}" class="btn btn-outline-secondary" style="width:300px;">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
</div>


@endsection

{{--
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>--}}


