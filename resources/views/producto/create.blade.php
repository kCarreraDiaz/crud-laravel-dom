{{--<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>CRUD - MOGO</title>
        <!-- BOOTSTRAP 5 -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

         <!-- Font Awesome CDN-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

    </head>
    <nav class="navbar navbar-dark bg-dark mb-4">
        <a href="/producto" class="navbar-brand">CRUD Mongo</a>
        <a href="{{ route('producto.show', 1) }}" class="navbar-brand">Landing</a>
    </nav>
    <body>--}}
@extends('layout.app')

@push('css')
    <style>
    </style>
@endpush
@section('content')
<div class="container">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Nuevo Producto</h1>
    </div>
    <div class="card mb-4" style="margin:auto;">
        <div class="card-body">
            <form class="" action="{{ url('/producto') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group py-2">
                    <input type="text" name="nombre" placeholder="Motorola X" class="form-control" required>
                </div>
                <div class="form-group py-2">
                    <textarea name="description" cols="80" class="form-control" placeholder="Celular Motorola X, Memoria 1TB..."></textarea>
                </div>
                <div class="form-group py-2">
                    <input type="text" name="precio" placeholder="S/. 1220" class="form-control">
                </div>
                <div class="form-group py-2">
                    <input type="text" name="stock" placeholder="8" class="form-control">
                </div>
                <div class="form-group py-2">
                    <input type="text" name="estado" placeholder="Disponible" class="form-control">
                </div>
                {{--<div class="form-group py-2">
                    <input type="file" name="imagen" placeholder="" class="form-control">
                </div>--}}
                <div class="form-group py-2">
                    <input type="file" name="file[]" multiple class="form-control">
                </div>
                <div class="form-group text-center py-4">
                    <button type="submit" class="btn btn-primary" style="width:300px;">Agregar</button>
                    <a href="{{ route('producto.index') }}" class="btn btn-outline-secondary" style="width:300px;">Cancelar</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection



{{--<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>--}}
