{{--<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>CRUD</title>
        <!-- BOOTSTRAP 5 -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

         <!-- Font Awesome CDN-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    </head>
    <nav class="navbar navbar-dark bg-dark mb-4">
        <a href="/producto" class="navbar-brand">CRUD</a>
    </nav>
    <body>--}}
@extends('layout.app')

@push('css')
    <style>
    </style>
@endpush
@section('content')
    <div class="container">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Ver productos</h1>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <div class="mb-3">
                    <label>Nombre</label>
                    <input type="text" name="nombre" value="{{ old('nombre', $producto->nombre) }}" class="form-control" placeholder="Ingrese el nombre" disabled />
                </div>
                <div class="form-group">
                    <label>description</label>
                    <input type="text" name="description" value="{{ old('description', $producto->description) }}" class="form-control" placeholder="Ingrese descripcion" disabled />
                </div>
                <div class="form-group">
                    <label>precio</label>
                    <input type="text" name="precio" value="{{ old('precio', $producto->precio) }}" class="form-control" placeholder="Ingrese el precio" disabled />
                </div>
                <div class="form-group">
                    <label>stock</label>
                    <input type="text" name="stock" value="{{ old('stock', $producto->stock) }}" class="form-control" placeholder="Ingrese el stock" disabled />
                </div>
                <div class="form-group">
                    <label>estado</label>
                    <input type="text" name="estado" value="{{ old('estado', $producto->estado) }}" class="form-control" placeholder="Ingrese el estado" disabled />
                </div>
                @if (!empty($images))
                    <div class="row py-4">
                        @foreach ($images as $image)
                        <div class="col-4" style="margin: auto;">
                            <img src="{{ old('', $image->url) }}" alt="" style="width: 200px;">   
                        </div>
                        @endforeach
                    </div>
                @endif
                <div class="panel-footer py-2">
                    <a href="{{ route('producto.index') }}" class="btn btn-primary" style="width:100px;">Atras</a>
                </div>
            </div>
        </div>
    </div>
    @endsection
    {{--
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>

--}}
