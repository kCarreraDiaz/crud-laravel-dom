<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>CRUD - MOGO</title>
        <!-- BOOTSTRAP 5 -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

         <!-- Font Awesome CDN-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

    </head>
    <nav class="navbar navbar-dark bg-dark mb-4">
        <a href="/producto" class="navbar-brand">CRUD Mongo</a>
        <a href="{{ route('producto.show', 1) }}" class="navbar-brand">Landing</a>
    </nav>
    <body>

<div class="container">
    <div class="col-md-10">
        <div class="card">
            <div class="card-body">
                <form class="" action="{{ url('/upload') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <h4>Nuevo Producto</h4>
                    
                    <div class="form-group py-2">
                        <input type="file" name="imagen" placeholder="" class="form-control">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary btn-block" style="width:300px;">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>





<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>
