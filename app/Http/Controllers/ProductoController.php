<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Image;
//use CloudinaryLabs\CloudinaryLaravel\MediaAlly;//va en el modelo
use Cloudinary;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$productos = Producto::paginate(5)
        $productos = Producto::all();
        return view('producto.index', compact('productos'));
        //return view('layout.app', compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('producto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $array_url = [];
        $producto = new Producto();
        $producto->nombre = $request->get('nombre');
        $producto->description = $request->get('description');
        $producto->precio = $request->get('precio');
        $producto->stock = $request->get('stock');
        $producto->estado = $request->get('estado');
        $cant = count($request->file('file'));
        for ($i=0; $i < $cant; $i++) {
            $uploadedFileUrl = Cloudinary::upload($request->file('file')[$i]->getRealPath())->getSecurePath();
            array_push($array_url, $uploadedFileUrl);
        }
        $producto->save();
        $id = $producto->id;
        for ($i=0; $i < count($array_url) ; $i++) { 
            $imagen = new Image();
            $imagen->url = $array_url[$i];
            $imagen->producto_id = $id;
            $imagen->estado = "ACTIVO";
            $imagen->save();
        }
        return redirect()->route('producto.index')->with('success','Producto creado correctamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = Producto::findOrFail($id);
        $images = Image::where('producto_id', $id)->get();
        return view('producto.ver', compact('producto', 'images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::findOrFail($id);
        return view('producto.edit', compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
        ]);
        $array_url = [];
        $producto = Producto::findOrFail($id);
        $producto->nombre = $request->get('nombre');
        $producto->description = $request->get('description');
        $producto->precio = $request->get('precio');
        $producto->stock = $request->get('stock');
        $producto->estado = $request->get('estado');
        $urlLast = $request->get('lastImage');
        if (!empty($request->file('imagenNueva'))) {
            $cant = count($request->file('imagenNueva'));
            // Eliminar imagenes anteriores de Cloudinary
            $images = Image::where('producto_id', $id)->get();
            if(!empty($images)) {
                for ($i=0; $i < count($images); $i++) { 
                    $a = explode("/", $images[$i]->url);
                    $aa = end($a);
                    $nom = explode(".", $aa);
                    $s= $nom[0];
                    Cloudinary::destroy($s);
                }
                // Eliminar registros anteriores de tabla Images
                for ($i=0; $i < count($images); $i++) { 
                    Image::findOrFail($images[$i]->id)->delete();
                }      
            }

            // Carga imagenes nuevas en array y cloudinary
            for ($i=0; $i < $cant; $i++) {
                $uploadedFileUrl = Cloudinary::upload($request->file('imagenNueva')[$i]->getRealPath())->getSecurePath();
                array_push($array_url, $uploadedFileUrl);
            }
            // Cargo imagenes en tabla Image
            for ($i=0; $i < count($array_url) ; $i++) { 
                $imagen = new Image();
                $imagen->url = $array_url[$i];
                $imagen->producto_id = $id;
                $imagen->estado = "ACTIVO";
                $imagen->save();
            }  
        }
        $producto->save();

        return redirect()->route('producto.index')->with('success', 'Contacto actualizado correctamente');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Eliminar imagenes anteriores de Cloudinary
        $images = Image::where('producto_id', $id)->get();
        if(!empty($images)) {
            for ($i=0; $i < count($images); $i++) { 
                $a = explode("/", $images[$i]->url);
                $aa = end($a);
                $nom = explode(".", $aa);
                $s= $nom[0];
                Cloudinary::destroy($s);
            }
            // Eliminar registros anteriores de tabla Images
            for ($i=0; $i < count($images); $i++) { 
                Image::findOrFail($images[$i]->id)->delete();
            } 
        }
        $producto = Producto::findOrFail($id)->delete();
        return back()->with("success", "Producto eliminado");
    }

}
