<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use CloudinaryLabs\CloudinaryLaravel\MediaAlly;//va en el modelo
class Producto extends Model
{
    use MediaAlly;
    protected $fillable = ['nombre', 'description','precio','stock','imagen','estado'];
}
